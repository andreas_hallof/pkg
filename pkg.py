#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, datetime, secrets, json, re

from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI, Header, Request, Response
# https://fastapi.tiangolo.com/advanced/templates/
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from binascii import hexlify, unhexlify
from base64 import b64encode

from tipkg.deviceauthentication import DA_Freshness

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


tags_metadata = [
    { "name": "PublicParameter",
      "description" : "Gibt die signierten aktuell gültigen PublicParameter" +
                      " des Vertrauensraums zurück. Die PublicParameter" +
                      " werden jährlich neu vom PKG erzeugt." },
    { "name": "PrivateKey",
      "description" : "Nach Übergabe eines für ein PKG-HSM verschlüsselten"+
                      " (Access_Token, AES-Rückgabeschlüssels), erhält ein"+
                      " Nutzer dessen private Schlüssel gesichert mittels"+
                      " des AES-Rückgabeschlüssels." },
    { "name": "HashACK",
      "description" : "Nach Übergabe eines für ein PKG-HSM verschlüsselten"+
                      " (Access_Token, Rückgabeschlüssel, Hashwert), erhält ein"+
                      " Nutzer eine vom PKG signierte Hashwertbestätigung"+
                      " (Fernsignatur-Light)." },
    { "name": "d27s",
      "description" : "Reserviert für zukünftigen Verwendungszweck" },
    { "name": "Random",
      "description" : "Infrastrukturleistung: als eine von mehreren Zufallsquellen"+
                      " gibt der PKG dem anfordernden Client 128 Byte Zufall"+
                      " für die Anreichung des Clientlokalen Entropie-Pools."},
    { "name": "Time",
      "description" : "Infrastrukturleistung: aktuelle Zeit (UTC)" },
     ] 
app = FastAPI(
        title="PKG",
        description="Private Key Generator so wie spezifiziert in gemSpec_PKG",
        version="1",
        openapi_tags=tags_metadata)

templates = Jinja2Templates(directory="templates")

class EncryptedRequest(BaseModel):
    EncryptedCommandString : str

@app.get("/PublicParameter", tags=["PublicParameter"])
def get_pp():
    with open("signed_public_parameter.json", "rt") as f:
        signed_public_parameter = f.read()
    return signed_public_parameter

@app.post("/PrivateKey", tags=["PrivateKey"])
def get_private_key(er : EncryptedRequest, User_ID : Optional[str] = Header(None)):
    return { "User-ID" : user_id }

@app.post("/HashACK", tags=["HashACK"])
def get_hash_ack(er : EncryptedRequest, User_ID : Optional[str] = Header(None)):
    return { "User-ID" : user_id }

# DeviceAuthenticationFreshness
@app.get("/d27s", tags=["d27s"])
def get_d27s():
    return DA_Freshness()

@app.get("/Random", tags=["Random"])
def get_random():
    return secrets.token_hex(128)

@app.get("/Time", tags=["Time"])
def get_time():
    return datetime.datetime.now().isoformat()

@app.get("/apikeys", response_class=HTMLResponse)
def Antwort(request: Request):

    with open("/dev/urandom", "rb") as f:
                data = f.read(16)

    return templates.TemplateResponse("apikeys.html", 
            { "request": request,
              "datum"  : datetime.datetime.now().isoformat()[0:19],
              "data_hex" : hexlify(data).decode(),
              "data_base64" : b64encode(data).decode(),
              "data_int" : str(int(hexlify(data).decode(), 16))
            })

@app.get("/pgdr", response_class=HTMLResponse)
def pgdr(request: Request):

    with open("/dev/urandom", "rb") as f:
                data = f.read(16)

    return templates.TemplateResponse("pgdr.html", 
            { "request": request,
              "datum"  : datetime.datetime.now().isoformat()[0:19],
              "data_hex" : hexlify(data).decode(),
              "data_base64" : b64encode(data).decode(),
              "data_int" : str(int(hexlify(data).decode(), 16))
            })

@app.post("/pgdr", response_class=HTMLResponse)
async def pgdr_post(request: Request):
    
    data = await request.body()
    #print(type(data), data)

    d2 = unhexlify(data)
    assert d2[0] == 1

    with open("pki/pgdr_vau_enc_priv_key.pem", "rb") as f:
        vau_private_key = serialization.load_pem_private_key(
            f.read(), password=None, backend=default_backend())

    pub_numbers = ec.EllipticCurvePublicNumbers(
        int.from_bytes(d2[1:1+32], byteorder='big'),
        int.from_bytes(d2[33:33+32], byteorder='big'),
        ec.SECP256R1())
    message_public_key = pub_numbers.public_key()

    # führe Elliptic-Curve-Diffie-Hellman durch
    shared_secret = vau_private_key.exchange(ec.ECDH(), message_public_key)
    #logging.info("shared_secret: {}".format(hexlify(shared_secret).decode()))
    print("HH:", hexlify(shared_secret))

    # Nun leite ich einen AES-Schlüssel für AES/GCM aus dem gemeinsamen
    # ECDH-Geheimnis ab.
    hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None,
                info=b'ecies-vau-transport', backend=default_backend())
    aes_key = hkdf.derive(shared_secret)
    print("HHH:", hexlify(aes_key))

    # Die Nachricht wird jetzt mit AES/GCM entchlüsselt.
    aesgcm = AESGCM(aes_key)
    iv = d2[1+32+32:1+32+32+12]
    assert len(iv) == 12
    ciphertext = d2[1+32+32+12:]
    plaintext_2 = aesgcm.decrypt(iv, ciphertext, associated_data=None)

    print("Plaintext_2=", plaintext_2)

    plaintext_2 = plaintext_2.decode()
    ar = plaintext_2.split()
    req_id = ar[2]
    ret_key = unhexlify(ar[3].encode())

    iv = os.urandom(12)
    print(plaintext_2[-20:])
    if re.match(".*GET /toc$", plaintext_2):
        answer_str = "1 " + req_id + " HIER VAU: Inhaltsverzeichnis der Akte ist ... HURRA funktioniert"
    else:
        answer_str = "1 " + req_id + " HIER VAU: Daten-Objekt 1 =  ... HURRA funktioniert"

    ciphertext_1 = AESGCM(ret_key).encrypt(iv, answer_str.encode(), associated_data=None)
    ciphertext_2 = hexlify(iv).decode() + hexlify(ciphertext_1).decode()

    return Response(content=json.dumps([ciphertext_2]), media_type="application/json")

