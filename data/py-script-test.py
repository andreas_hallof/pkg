#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, json, os, sys, time, secrets
from binascii import hexlify, unhexlify

import asyncio
from pyodide.http import pyfetch, FetchResponse
from typing import Optional


from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


my_token = """
eyJhbGciOiAiRVMyNTYiLCAieDVjIjogWyJNSUlDQWpDQ0Fha0NGRmUyTVRZVXJYMjhVN2FYSm9XemIr
WEYzcTlyTUFvR0NDcUdTTTQ5QkFNQ01IQXhDekFKQmdOVkJBWVRBa1JGTVE4d0RRWURWUVFJREFaQ1pY
SnNhVzR4RHpBTkJnTlZCQWNNQmtKbGNteHBiakVRTUE0R0ExVUVDZ3dIWjJWdFlYUnBhekVRTUE0R0Ex
VUVDd3dIWjJWdFlYUnBhekViTUJrR0ExVUVBd3dTUzI5dGNHOXVaVzUwWlc0dFVFdEpMVU5CTUI0WERU
SXlNRE13TVRFek5USXpPRm9YRFRJek1ETXdNVEV6TlRJek9Gb3dnWll4Q3pBSkJnTlZCQVlUQWtSRk1R
OHdEUVlEVlFRSURBWkNaWEpzYVc0eER6QU5CZ05WQkFjTUJrSmxjbXhwYmpFWU1CWUdBMVVFQ2d3UFpW
QkJJRUZyZEdWdWMzbHpkR1Z0TVNFd0h3WURWUVFMREJoTGIyMXdiMjVsYm5SbElFRjFkRzl5YVhOcFpY
SjFibWN4S0RBbUJnTlZCQU1NSDJWUVFTMUJVeUJMYjIxd2IyNWxiblJsSUVGMWRHOXlhWE5wWlhKMWJt
Y3dXakFVQmdjcWhrak9QUUlCQmdrckpBTURBZ2dCQVFjRFFnQUVraytOUDY4QWVVMXR6cis1UEFVZWtw
WjNyTGtqU2h2ODhVSkpEQkFTSDh4emlYTU84TW1aU0p5SHZXRng0NlB4Rm80SU5CZmErcmh2dXBUTkhm
bm9WakFLQmdncWhrak9QUVFEQWdOSEFEQkVBaUI5ZXpNMEtBckIwWU9XVi9YeGV6eDhYNTVtdi9ZN1Rl
d2dxRUJSS0psV2xnSWdJckdZa1QvdStZWjdieStVOE5lVHAzN28xWkZRMkJ2NStLL3paZGlmQUFjPSJd
fQ.eyJ3biI6ICJDQjlxVzVWYnFpMzREYkZuanJNMmNibUkvY1d3b3J4YlRKQ1VYcEQ1eVpzPSIsICJpY
XQiOiAxNjUzOTM0MDQ5LjE4NDUxOSwgImV4cCI6IDE2NTQwMjA0NDkuMTg0NTE5LCAiaXNzIjogImh0d
HA6Ly9hdXRob3JpemF0aW9uLmFrdGVuc3lzdGVtLnRpIn0.MEUCIQCoQYoU92W4CjODoGkV7TYvFony4
oTMIJGT5T98xiJz_wIgO52nZQTKrQDe6YqPyTPMCCU0s4hp6wQmjXQxnS2tDr4""".replace("\n", "")

pgdr_vau_cert = """-----BEGIN CERTIFICATE-----
MIICIjCCAcegAwIBAgIUAnZOHbgzNGNX/bpG2BvvLEK8S0EwCgYIKoZIzj0EAwIw
ZjELMAkGA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGlu
MRAwDgYDVQQKDAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMREwDwYDVQQDDAhw
R0RSIFZBVTAeFw0yMjA3MTUxNTMxMTlaFw0yMzA3MTUxNTMxMTlaMGYxCzAJBgNV
BAYTAkRFMQ8wDQYDVQQIDAZCZXJsaW4xDzANBgNVBAcMBkJlcmxpbjEQMA4GA1UE
CgwHZ2VtYXRpazEQMA4GA1UECwwHZ2VtYXRpazERMA8GA1UEAwwIcEdEUiBWQVUw
WTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATf0MfQGKMGoYRgJ6merk7xsA3j1X0G
NJ7AfNkb0qH0U+HJChvgjcDmzBoQzCsgfViFhE0HCWccpX5Xxlm9b4P+o1MwUTAd
BgNVHQ4EFgQUo1s6ZDBzPr3qxaPiNnzsNYED09cwHwYDVR0jBBgwFoAUo1s6ZDBz
Pr3qxaPiNnzsNYED09cwDwYDVR0TAQH/BAUwAwEB/zAKBggqhkjOPQQDAgNJADBG
AiEA2N+qZkKYmz7dsYWxaepi37GwhXleuRXM3Yt9zZWJ62MCIQDUoQ1sGf93YA1j
+K/UlOb7zlYBPTC5EhdIq1c6FU9ooA==
-----END CERTIFICATE-----""".encode()

vau_public_key = x509.load_pem_x509_certificate(pgdr_vau_cert, default_backend()).public_key()
#vau_public_key = ECC_Public_Key_TSL_Signer_CA_TU = ec.EllipticCurvePublicNumbers(
#                    x=0xdfd0c7d018a306a1846027a99eae4ef1b00de3d57d06349ec07cd91bd2a1f453,
#                    y=0xe1c90a1be08dc0e6cc1a10cc2b207d5885844d0709671ca57e57c659bd6f83fe,
#                    curve=ec.SECP256R1())

def gen_enc_req(token: str, req_id: str, answer_key: str, cmd: str) -> str:
    # für jede Nachricht neu zu erzeugen:
    private_key = ec.generate_private_key(ec.SECP256R1(), default_backend())
    pn = private_key.public_key().public_numbers()
    shared_secret = private_key.exchange(ec.ECDH(), vau_public_key)
    #console.log("++h2")
    #console.log(hexlify(shared_secret).decode())
    #console.log("++h3")
    hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None, info=b'ecies-vau-transport', backend=default_backend())
    aes_key = hkdf.derive(shared_secret)
    #console.log(hexlify(aes_key).decode())
    #console.log("++h4")
    plaintext = "1 " + token + " " + req_id + " " + answer_key + " " + cmd
    plaintext = plaintext.encode()
    iv = os.urandom(12)
    ciphertext = AESGCM(aes_key).encrypt(iv, plaintext, associated_data=None)
    x_str = "{:x}".format(pn.x).zfill(64)
    y_str = "{:x}".format(pn.y).zfill(64)

    # der zu übertragene Ciphertext nach A_20161-* ist damit (als hexdump dargestellt):
    ciphertext = "01" + x_str + y_str + hexlify(iv).decode() + hexlify(ciphertext).decode()

    return ciphertext

def dec_response(req_id: str, answer_key: str, data: str) -> str:

    d1 = unhexlify(data)
    iv = d1[:12]
    ct = d1[12:]

    pt = AESGCM(unhexlify(answer_key)).decrypt(iv, ct, associated_data=None).decode()

    console.log(pt)

    return pt


async def request(url:str, method:str = "GET", body:Optional[str] = None,
 headers:Optional[dict[str,str]] = None) -> FetchResponse:
    """
    Async request function. Pass in Method and make sure to await!
    Parameters:
        method: str = {"GET", "POST", "PUT", "DELETE"} from javascript global fetch())
        body: str = body as json string. Example, body=json.dumps(my_dict)
        header: dict[str,str] = header as dict, will be converted to string... 
            Example, header:json.dumps({"Content-Type":"application/json"})
    Return: 
        response: pyodide.http.FetchResponse = use with .status or await.json(), etc.
    """
    kwargs = {"method":method, "mode":"cors"}
    if body and method not in ["GET", "HEAD"]:
        kwargs["body"] = body
    if headers:
        kwargs["headers"] = headers

    response = await pyfetch(url, **kwargs)
    return response

def huebsch_hex(s: str) -> str:
  
    ret_s = ""
    for i in range(0, len(s)):
        ret_s += s[i]
        if (i%10 == 0) and (i!=0):
            ret_s += " "
    if len(ret_s)>500:
        ret_s = ret_s[0:500] + " ..."

    return ret_s

async def my_get_toc(*args, **kwargs):
    result_enc_req = Element("EncReq")
    result_enc_req.write("START " + datetime.datetime.now().isoformat())
    result_enc_resp = Element("EncResp")
    result_toc = Element("AkteTOC")

    #console.log("HI", args, kwargs)
    #response = await pyfetch(url="https://tipkg.de/Random", method="GET")
    #output = f"GET request=> status:{response.status}, json:{await response.json()}"
    #result_place.write(output + datetime.datetime.now().isoformat())

    headers = {"Content-type": "application/json"}
    aes_answer_key = secrets.token_hex(16)
    req_id = secrets.token_hex(16)
    my_data = gen_enc_req(my_token, req_id, aes_answer_key, "GET /toc")
    console.log(my_data)
    my_data_fmt = huebsch_hex(my_data)
    result_enc_req.write(datetime.datetime.now().isoformat() + " " + my_data_fmt)
    # https://github.com/pyscript/pyscript/pull/151/files
    new_post = await request("https://tipkg.de/pgdr", body=my_data, method="POST", headers=headers)
    response_data = await new_post.json()
    if isinstance(response_data, list):
        response_data = response_data[0]

    console.log(response_data)
    my_answer = dec_response(req_id, aes_answer_key, response_data)
    resp2 = huebsch_hex(response_data)
    output = f"POST request=> status: {new_post.status}, Response = {resp2}"
    result_enc_resp.write(datetime.datetime.now().isoformat() + " " + output)

    result_toc.write(my_answer[35:] + " (" + datetime.datetime.now().isoformat() + ")")

    return True

async def my_get_first_do(*args, **kwargs):
    result_enc_req = Element("EncReq")
    result_enc_req.write("START " + datetime.datetime.now().isoformat())
    result_enc_resp = Element("EncResp")
    result_first_do = Element("FirstDO")

    headers = {"Content-type": "application/json"}
    aes_answer_key = secrets.token_hex(16)
    req_id = secrets.token_hex(16)
    my_data = gen_enc_req(my_token, req_id, aes_answer_key, "GET /dataobject/1")
    console.log(my_data)
    my_data_fmt = huebsch_hex(my_data)
    result_enc_req.write(datetime.datetime.now().isoformat() + " " + my_data_fmt)
    # https://github.com/pyscript/pyscript/pull/151/files
    new_post = await request("https://tipkg.de/pgdr", body=my_data, method="POST", headers=headers)
    response_data = await new_post.json()
    if isinstance(response_data, list):
        response_data = response_data[0]
        console.log("H4: JA liste")

    console.log(response_data)
    my_answer = dec_response(req_id, aes_answer_key, response_data)
    resp2 = huebsch_hex(response_data)
    output = f"POST request=> status: {new_post.status}, Response = {resp2}"
    result_enc_resp.write(datetime.datetime.now().isoformat() + " " + output)

    result_first_do.write(my_answer[35:] + " (" + datetime.datetime.now().isoformat() + ")")

    return True


print("Aktuell verwendete Laufzeitumgebung im Webbrowser:", sys.version)
token_place = Element("MyToken")
token_place.write(my_token[:40]+"...")

