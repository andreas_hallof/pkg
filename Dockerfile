FROM tiangolo/uvicorn-gunicorn:python3.8

#RUN pip install --no-cache-dir uvicorn
#RUN pip install --no-cache-dir fastapi

MAINTAINER Andreas Hallof, andreas.hallof@googlemail.com

COPY . /app
WORKDIR /app

#RUN apt-get update && apt-get install openssl

RUN apt-get update
RUN apt-get --yes install apt-utils
RUN apt-get --yes install libmpc-dev libmpfr-dev libmpfr-doc 
RUN apt-get --yes install libldap2-dev libsasl2-dev


RUN pip install pyOpenSSL 
RUN pip install -r requirements.txt

#CMD ["echo", "Hallo Welt!"]
CMD ["uvicorn", "pkg:app" ]

EXPOSE 8000


