#! /usr/bin/python3

import datetime, hmac, secrets

C_SECRET = secrets.token_bytes(32)

def DA_Freshness() -> str:
    """
    Alle 10' gibt es einen neuen vom Client nicht vorhersagbaren 256-Bit 
    Frischewert vom FD. 

    Diese oder der vorhergehende (10' zuvor also) muss dann in das vom
    Gerät in Rahmen der Geräte-Nutzer-Authentisierung in die Proof-of-Posession
    eingebracht werden.

    """

    x = datetime.datetime.now().isoformat()[:15].encode()
    # hier kommt also bspw. '2020-11-15T21:5' raus

    y = hmac.new(C_SECRET, msg=x, digestmod='sha256')

    return y.hexdigest()

