# Raspberry Pi Model 4B

## RSA

	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ openssl speed rsa2048
	Doing 2048 bits private rsa's for 10s: 1439 2048 bits private RSA's in 9.99s
	Doing 2048 bits public rsa's for 10s: 65764 2048 bits public RSA's in 10.00s
	OpenSSL 1.1.1d  10 Sep 2019
	built on: Mon Apr 27 09:55:40 2020 UTC
	options:bn(64,32) rc4(char) des(long) aes(partial) blowfish(ptr) 
	compiler: gcc -fPIC -pthread -Wa,--noexecstack -Wall -D__ARM_MAX_ARCH__=7 -Wa,--noexecstack -g -O2 -fdebug-prefix-map=/home/pi/work/new=. -fstack-protector-strong -Wformat -Werror=format-security -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time -D_FORTIFY_SOURCE=2
			  sign    verify    sign/s verify/s
	rsa 2048 bits 0.006942s 0.000152s    144.0   6576.4
	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ cat temp 
	35537

## AES

	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ cat temp 
	36511
	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ openssl speed aes
	Doing aes-128 cbc for 3s on 16 size blocks: 13800147 aes-128 cbc's in 3.00s
	Doing aes-128 cbc for 3s on 64 size blocks: 3787510 aes-128 cbc's in 2.99s
	Doing aes-128 cbc for 3s on 256 size blocks: 986456 aes-128 cbc's in 3.00s
	Doing aes-128 cbc for 3s on 1024 size blocks: 249077 aes-128 cbc's in 3.00s
	Doing aes-128 cbc for 3s on 8192 size blocks: 31157 aes-128 cbc's in 3.00s
	Doing aes-128 cbc for 3s on 16384 size blocks: 15567 aes-128 cbc's in 2.99s
	Doing aes-192 cbc for 3s on 16 size blocks: 12348824 aes-192 cbc's in 3.00s
	Doing aes-192 cbc for 3s on 64 size blocks: 3299978 aes-192 cbc's in 2.99s
	Doing aes-192 cbc for 3s on 256 size blocks: 854886 aes-192 cbc's in 3.00s
	Doing aes-192 cbc for 3s on 1024 size blocks: 215512 aes-192 cbc's in 3.00s
	Doing aes-192 cbc for 3s on 8192 size blocks: 27008 aes-192 cbc's in 3.00s
	Doing aes-192 cbc for 3s on 16384 size blocks: 13488 aes-192 cbc's in 3.00s
	Doing aes-256 cbc for 3s on 16 size blocks: 11018800 aes-256 cbc's in 3.00s
	Doing aes-256 cbc for 3s on 64 size blocks: 2927107 aes-256 cbc's in 3.00s
	Doing aes-256 cbc for 3s on 256 size blocks: 743519 aes-256 cbc's in 3.00s
	Doing aes-256 cbc for 3s on 1024 size blocks: 189427 aes-256 cbc's in 2.99s
	Doing aes-256 cbc for 3s on 8192 size blocks: 23788 aes-256 cbc's in 3.00s
	Doing aes-256 cbc for 3s on 16384 size blocks: 11883 aes-256 cbc's in 3.00s
	OpenSSL 1.1.1d  10 Sep 2019
	built on: Mon Apr 27 09:55:40 2020 UTC
	options:bn(64,32) rc4(char) des(long) aes(partial) blowfish(ptr) 
	compiler: gcc -fPIC -pthread -Wa,--noexecstack -Wall -D__ARM_MAX_ARCH__=7 -Wa,--noexecstack -g -O2 -fdebug-prefix-map=/home/pi/work/new=. -fstack-protector-strong -Wformat -Werror=format-security -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time -D_FORTIFY_SOURCE=2
	The 'numbers' are in 1000s of bytes per second processed.
	type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes
	aes-128 cbc      73600.78k    81070.45k    84177.58k    85018.28k    85079.38k    85300.91k
	aes-192 cbc      65860.39k    70634.98k    72950.27k    73561.43k    73749.85k    73662.46k
	aes-256 cbc      58766.93k    62444.95k    63446.95k    64874.00k    64957.10k    64897.02k
	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ cat temp
	37972

# Uralt-PC

mehr als 10 Jahre alt, Intel Core2 Quad Q9550 @ 2.83GHz

## RSA

	$ openssl speed rsa2048
        Doing 2048 bits private rsa's for 10s: 4325 2048 bits private RSA's in 9.99s
        Doing 2048 bits public rsa's for 10s: 145179 2048 bits public RSA's in 10.00s
        OpenSSL 1.1.1i  8 Dec 2020
        built on: Sat Dec 12 06:34:39 2020 UTC
        options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr) 
        compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -Wa,--noexecstack -D_FORTIFY_SOURCE=2 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -D_FORTIFY_SOURCE=2
                          sign    verify    sign/s verify/s
        rsa 2048 bits 0.002310s 0.000069s    432.9  14517.9

## AES

	$ openssl speed aes
        Doing aes-128 cbc for 3s on 16 size blocks: 22845882 aes-128 cbc's in 3.00s
        Doing aes-128 cbc for 3s on 64 size blocks: 6230756 aes-128 cbc's in 2.99s
        Doing aes-128 cbc for 3s on 256 size blocks: 1591288 aes-128 cbc's in 3.00s
        Doing aes-128 cbc for 3s on 1024 size blocks: 400121 aes-128 cbc's in 3.00s
        Doing aes-128 cbc for 3s on 8192 size blocks: 49831 aes-128 cbc's in 3.00s
        Doing aes-128 cbc for 3s on 16384 size blocks: 24917 aes-128 cbc's in 3.00s
        Doing aes-192 cbc for 3s on 16 size blocks: 19986409 aes-192 cbc's in 3.00s
        Doing aes-192 cbc for 3s on 64 size blocks: 5359400 aes-192 cbc's in 2.99s
        Doing aes-192 cbc for 3s on 256 size blocks: 1362643 aes-192 cbc's in 3.00s
        Doing aes-192 cbc for 3s on 1024 size blocks: 342066 aes-192 cbc's in 3.00s
        Doing aes-192 cbc for 3s on 8192 size blocks: 42639 aes-192 cbc's in 3.00s
        Doing aes-192 cbc for 3s on 16384 size blocks: 21317 aes-192 cbc's in 3.00s
        Doing aes-256 cbc for 3s on 16 size blocks: 17658869 aes-256 cbc's in 3.00s
        Doing aes-256 cbc for 3s on 64 size blocks: 4678891 aes-256 cbc's in 2.99s
        Doing aes-256 cbc for 3s on 256 size blocks: 1188455 aes-256 cbc's in 3.00s
        Doing aes-256 cbc for 3s on 1024 size blocks: 298253 aes-256 cbc's in 3.00s
        Doing aes-256 cbc for 3s on 8192 size blocks: 37193 aes-256 cbc's in 3.00s
        Doing aes-256 cbc for 3s on 16384 size blocks: 18596 aes-256 cbc's in 3.00s
        OpenSSL 1.1.1i  8 Dec 2020
        built on: Sat Dec 12 06:34:39 2020 UTC
        options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr) 
        compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -Wa,--noexecstack -D_FORTIFY_SOURCE=2 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -D_FORTIFY_SOURCE=2
        The 'numbers' are in 1000s of bytes per second processed.
        type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes
        aes-128 cbc     121844.70k   133367.35k   135789.91k   136574.63k   136071.85k   136080.04k
        aes-192 cbc     106594.18k   114716.25k   116278.87k   116758.53k   116432.90k   116419.24k
        aes-256 cbc      94180.63k   100150.18k   101414.83k   101803.69k   101561.69k   101558.95k

