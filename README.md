# PKG

PoC für den Private Key Generator (PKG) 

Der PKG berechnet als Teil eines "Identity Based Encryption Systems" die privaten Schlüssel
der Empfänger.

Mit dem PKG kann man ebenfalls im Kontext Data-Authentication einen Hashwert inkl. Zeitinformationen 
bestätigen lassen (quasi als Fernsignatur-Light).

Ebenfalls ist Beispielcode für die Ver- und Entschlüsselung mittels des IBE hier zu finden.


## PoC Instanz

Auf [https://tipkg.de](https://tipkg.de/) gibt es eine laufende Instanz zum ausprobieren.

## Docker-Image 

Eine lokale Instanz kann man auch leicht mit dem [Dockerimage](https://hub.docker.com/r/andreashallof/tipkg)
starten

    docker pull andreashallof/tipkg
    docker run -p 8000:8000 --net=host tipkg:0.1

Und jetzt mit einen Browser auf die URL http://127.0.0.1:8000/docs gehen.

## Instanz lokal starten

Wenn man tiefer einsteigen möchte, empfiehlt es sich traditionell den pkg-server lokal
zu starten, so dass man leichter den Code für Experimente verändern kann:

0) Ggf. notwendige python-Bibliotheken intstallieren

    pip install -r requirements.txt

1) via uvicron oder hypercorn starten

    uvicorn pkg:app --reload --uds /tmp/uvicorn.sock

## python-Client

## c-Client

## java-Client

## rust-Client

## TSL-Eintrag

Analog zu den von der gematik definierten TSL-Einträge-Struktur für die
CVC-Root-CA-Zertifikate und die Cross-CVC-Root-CA-Zertifikate definieren wir

    <TSPService>
      <ServiceInformation>
        <ServiceTypeIdentifier> http://uri.telematik/TrstSvc/Svctype/PKG </ServiceTypeIdentifier>
        <ServiceName>
          <Name xml:lang="DE">PKG der Telematikinfrastruktur</Name>
        </ServiceName>
        <ServiceDigitalIdentity>
          <DigitalId>
            <Other>
                <ns:PKGPublicParameter xmlns:ns="http://uri.etsi.org/02231/v2#"> 
                    WwogIHsgImlkIiA6ICJCb25laC1GcmFua2xpbi1SRkMtNTA5MSIsCiAgICAiY3VydmVfaWQiIDogInh5eiIsCiAgICAicCIgOiAiMHhhMCIsCiAgICAicSIgOiAiMHhiMCIsCiAgICAicG9pbnRQIiA6IFsgIjB4MSIsICIweDIiIF0sCiAgICAicG9pbnRQcHViIiA6IFsgIjB4MyIgLCAiMHg0IiBdLAogICAgImhhc2hfaWQiIDogIjEyMzQiCiAgfSwKICB7ICJpZCIgOiAiYW5vdGhlcl9pYmVfY3J5cHRvc3lzdGVtIiwKICAgICJub3Rfa25vd195ZXQiIDogIjEiCiAgfQpd
                </ns:PKGPublicParameter>
            </Other>
          </DigitalId>
        </ServiceDigitalIdentity>
        <ServiceStatus> http://uri.etsi.org/TrstSvc/Svcstatus/inaccord</ServiceStatus>
        <StatusStartingTime> 2020-01-21T10:00:00Z</StatusStartingTime>
        <ServiceSupplyPoints>
          <ServiceSupplyPoint> http://ocsp00.gematik.invalid/not-used</ServiceSupplyPoint>
        </ServiceSupplyPoints>
        <ServiceInformationExtensions>
          <Extension Critical="false">
            <ns:ExtensionOID xmlns:ns="http://uri.etsi.org/02231/v2#"> 1.2.276.0.76.4.xxx </ns:ExtensionOID>
            <ns:ExtensionValue xmlns:ns="http://uri.etsi.org/02231/v2#"> oid_public_parameter </ns:ExtensionValue>
          </Extension>
        </ServiceInformationExtensions>
      </ServiceInformation>
    </TSPService>

## Public Parameter

JSON-Array. Ein Client verwendet das erste Verfahren, dass er unterstützt.

    [
      { "id" : "BF2020-05",
        "cryptosystem" : "Boneh-Franklin-RFC-5091",
        "curve_id" : "y^2=x^3+1",
        "p" : "0xffffffffffffffffffffc0000000000000000000000000000000000000000001",
        "q" : "0x17fffffffffffffffffffa000000000000000000000000000000000000000000180000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000042cbffffffffffffffffef4d000000000000000000000000000000000000000042cb 1536.584962500721",
        "pointP" : [ "0x1", "0x2" ],
        "pointPpub" : [ "0x3" , "0x4" ],
        "hash_id" : "1234"
      },
      { "id" : "another_ibe_cryptosystem",
        "not_know_yet" : "1"
      }
    ]

