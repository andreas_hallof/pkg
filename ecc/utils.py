#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from hashlib import sha256

# RFC 5091, 4.1.1
def HashToRangeRFC(s: bytes, n: int) -> int:
    HASHLEN = 32
    v = [0, 0, 0]
    h = [b'\x00'*HASHLEN, '', '']
    t = ['', '', '']
    for i in range(1, 3):
        t[i] = h[i-1] + s
        h[i] = sha256(t[i]).digest()
        tmp = int.from_bytes(h[i], byteorder='big', signed=False)
        v[i] = 256**HASHLEN * v[i-1] + tmp

    return v[2] % n

def HashToRange(s: bytes, n: int) -> int:
    HASHLEN = 32
    h1 = sha256(b'\x00'*HASHLEN + s).digest()
    v1 = int.from_bytes(h1, byteorder='big', signed=False)
    h2 = sha256(h1 + s).digest()
    v2 = int.from_bytes(h2, byteorder='big', signed=False)
    v = 256**HASHLEN * v1 + v2
    return v % n

if __name__ == '__main__':
    test_strings = [ b"This ASCII string without null-terminator", b"test1", b"test2" ]
    test_n = [ 2**256, 80, 2**256 -20  ]
    for s in test_strings:
        for n in test_n:
            assert HashToRangeRFC(s, n) == HashToRange(s, n)

