#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, json
from math import log2
from gmpy2 import  is_prime




q = 2**256 - 2**174 + 1

assert is_prime(q)

print(f"q=0x{q:x}", log2(q))

r = 2**(1533-256)

p = 12 * r * q - 1
while not is_prime(p):
    p = 12 * r * q - 1
    r += 1

print(f"r=0x{r:x}")

assert is_prime(p)

print(f"p=0x{p:x}", log2(p))

