#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, json, os, sys, time
from binascii import hexlify

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


my_token = """
eyJhbGciOiAiRVMyNTYiLCAieDVjIjogWyJNSUlDQWpDQ0Fha0NGRmUyTVRZVXJYMjhVN2FYSm9XemIr
WEYzcTlyTUFvR0NDcUdTTTQ5QkFNQ01IQXhDekFKQmdOVkJBWVRBa1JGTVE4d0RRWURWUVFJREFaQ1pY
SnNhVzR4RHpBTkJnTlZCQWNNQmtKbGNteHBiakVRTUE0R0ExVUVDZ3dIWjJWdFlYUnBhekVRTUE0R0Ex
VUVDd3dIWjJWdFlYUnBhekViTUJrR0ExVUVBd3dTUzI5dGNHOXVaVzUwWlc0dFVFdEpMVU5CTUI0WERU
SXlNRE13TVRFek5USXpPRm9YRFRJek1ETXdNVEV6TlRJek9Gb3dnWll4Q3pBSkJnTlZCQVlUQWtSRk1R
OHdEUVlEVlFRSURBWkNaWEpzYVc0eER6QU5CZ05WQkFjTUJrSmxjbXhwYmpFWU1CWUdBMVVFQ2d3UFpW
QkJJRUZyZEdWdWMzbHpkR1Z0TVNFd0h3WURWUVFMREJoTGIyMXdiMjVsYm5SbElFRjFkRzl5YVhOcFpY
SjFibWN4S0RBbUJnTlZCQU1NSDJWUVFTMUJVeUJMYjIxd2IyNWxiblJsSUVGMWRHOXlhWE5wWlhKMWJt
Y3dXakFVQmdjcWhrak9QUUlCQmdrckpBTURBZ2dCQVFjRFFnQUVraytOUDY4QWVVMXR6cis1UEFVZWtw
WjNyTGtqU2h2ODhVSkpEQkFTSDh4emlYTU84TW1aU0p5SHZXRng0NlB4Rm80SU5CZmErcmh2dXBUTkhm
bm9WakFLQmdncWhrak9QUVFEQWdOSEFEQkVBaUI5ZXpNMEtBckIwWU9XVi9YeGV6eDhYNTVtdi9ZN1Rl
d2dxRUJSS0psV2xnSWdJckdZa1QvdStZWjdieStVOE5lVHAzN28xWkZRMkJ2NStLL3paZGlmQUFjPSJd
fQ.eyJ3biI6ICJDQjlxVzVWYnFpMzREYkZuanJNMmNibUkvY1d3b3J4YlRKQ1VYcEQ1eVpzPSIsICJpY
XQiOiAxNjUzOTM0MDQ5LjE4NDUxOSwgImV4cCI6IDE2NTQwMjA0NDkuMTg0NTE5LCAiaXNzIjogImh0d
HA6Ly9hdXRob3JpemF0aW9uLmFrdGVuc3lzdGVtLnRpIn0.MEUCIQCoQYoU92W4CjODoGkV7TYvFony4
oTMIJGT5T98xiJz_wIgO52nZQTKrQDe6YqPyTPMCCU0s4hp6wQmjXQxnS2tDr4""".replace("\n", "")

pgdr_vau_cert = """-----BEGIN CERTIFICATE-----
MIICIjCCAcegAwIBAgIUAnZOHbgzNGNX/bpG2BvvLEK8S0EwCgYIKoZIzj0EAwIw
ZjELMAkGA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGlu
MRAwDgYDVQQKDAdnZW1hdGlrMRAwDgYDVQQLDAdnZW1hdGlrMREwDwYDVQQDDAhw
R0RSIFZBVTAeFw0yMjA3MTUxNTMxMTlaFw0yMzA3MTUxNTMxMTlaMGYxCzAJBgNV
BAYTAkRFMQ8wDQYDVQQIDAZCZXJsaW4xDzANBgNVBAcMBkJlcmxpbjEQMA4GA1UE
CgwHZ2VtYXRpazEQMA4GA1UECwwHZ2VtYXRpazERMA8GA1UEAwwIcEdEUiBWQVUw
WTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATf0MfQGKMGoYRgJ6merk7xsA3j1X0G
NJ7AfNkb0qH0U+HJChvgjcDmzBoQzCsgfViFhE0HCWccpX5Xxlm9b4P+o1MwUTAd
BgNVHQ4EFgQUo1s6ZDBzPr3qxaPiNnzsNYED09cwHwYDVR0jBBgwFoAUo1s6ZDBz
Pr3qxaPiNnzsNYED09cwDwYDVR0TAQH/BAUwAwEB/zAKBggqhkjOPQQDAgNJADBG
AiEA2N+qZkKYmz7dsYWxaepi37GwhXleuRXM3Yt9zZWJ62MCIQDUoQ1sGf93YA1j
+K/UlOb7zlYBPTC5EhdIq1c6FU9ooA==
-----END CERTIFICATE-----""".encode()

vau_public_key = ECC_Public_Key_TSL_Signer_CA_TU = ec.EllipticCurvePublicNumbers(
                    x=0xdfd0c7d018a306a1846027a99eae4ef1b00de3d57d06349ec07cd91bd2a1f453,
                    y=0xe1c90a1be08dc0e6cc1a10cc2b207d5885844d0709671ca57e57c659bd6f83fe,
                    curve=ec.SECP256R1())
vau_public_key = x509.load_pem_x509_certificate(pgdr_vau_cert, default_backend()).public_key()

def gen_enc_req():
    # für jede Nachricht neu zu erzeugen:
    private_key = ec.generate_private_key(ec.SECP256R1(), default_backend())
    pn = private_key.public_key().public_numbers()
    shared_secret = private_key.exchange(ec.ECDH(), vau_public_key)
    #shared_secret = b"1234567890"
    hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None, info=b'ecies-vau-transport', backend=default_backend())
    aes_key = hkdf.derive(shared_secret)
    plaintext = "Hallo Test".encode()
    iv = os.urandom(12)
    ciphertext = AESGCM(aes_key).encrypt(iv, plaintext, associated_data=None)
    x_str = "{:x}".format(pn.x).zfill(64)
    y_str = "{:x}".format(pn.y).zfill(64)

    # der zu übertragene Ciphertext nach A_20161-* ist damit (als hexdump dargestellt):
    ciphertext = "01" + x_str + y_str + hexlify(iv).decode() + hexlify(ciphertext).decode()

    return ciphertext

if __name__ == '__main__':
    print(gen_enc_req())

