Unter [https://github.com/cryptid-org/cryptid-native/blob/master/src/identity-based/encryption/boneh-franklin/BonehFranklinIdentityBasedEncryption.c](
https://github.com/cryptid-org/cryptid-native/blob/master/src/identity-based/encryption/boneh-franklin/BonehFranklinIdentityBasedEncryption.c)
gibt es eine C-Implementierung des Boneh-Franklin IBE nach RFC-5091. Die
mathematischen Operationen werden mit der [gmp-Bibliothek](xxx) umgesetzt.

Interessant ebenfalls [Tepla](http://www.cipher.risk.tsukuba.ac.jp/tepla/download_e.html)
und die [PBC-Library](https://crypto.stanford.edu/pbc/manual/).

