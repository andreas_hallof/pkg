#! /usr/bin/env bash

openssl ecparam -name prime256v1 -genkey -out pgdr_vau_enc_priv_key.pem

openssl req -x509 -key pgdr_vau_enc_priv_key.pem \
    -outform pem \
    -out pgdr_vau_enc_cert.pem -days 365 \
    -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=pGDR VAU"

